from django.core.management.base import BaseCommand
from telegram.ext import Updater, CommandHandler

from config import settings
from app.internal.transport.bot.handlers import start, set_phone, me


class Command(BaseCommand):
    def handle(self, *args, **options):
        updater = Updater(settings.TG_TOKEN, use_context=True)
        updater.dispatcher.add_handler(CommandHandler("start", start))
        updater.dispatcher.add_handler(CommandHandler("set_phone", set_phone))
        updater.dispatcher.add_handler(CommandHandler("me", me))
        updater.start_polling()
        updater.idle()
