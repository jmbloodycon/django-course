from rest_framework import serializers
from app.models import TelegramUser


class TgUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = TelegramUser
        fields = [
            'id',
            'username',
            'phone',
            'full_name',
        ]
