from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from app.internal.models.admin_user import AdminUser
from app.internal.models.t_user import TelegramUser


@admin.register(AdminUser)
class AdminUserAdmin(UserAdmin):
    pass


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    pass
