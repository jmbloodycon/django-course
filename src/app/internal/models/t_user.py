from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class TelegramUser(models.Model):
    username = models.CharField(max_length=32)
    phone = PhoneNumberField(blank=True)
    full_name = models.CharField(max_length=255)
