from django.urls import path

from app.internal.transport.rest.handlers import TelegramUserGetView

urlpatterns = [
    path('me/<int:pk>/', TelegramUserGetView.as_view()),
]
