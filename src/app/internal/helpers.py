import re


PHONE_PATTERN = re.compile(
    '^((8|\+374|\+994|\+995|\+375|\+7|\+380|\+38|\+996|\+998|\+993)[\- ]?)?\(?\d{3,5}\)?[\- ]?\d{1}[\- ]?\d{1}[\- ]?'
    '\d{1}[\- ]?\d{1}[\- ]?\d{1}(([\- ]?\d{1})?[\- ]?\d{1})?$'
) # noqa


def get_correct_phone(str_phone):
    if phone := re.fullmatch(PHONE_PATTERN, str_phone):
        return phone.string

    raise ValueError
