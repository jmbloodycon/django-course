from django.http import HttpResponseNotFound, JsonResponse, HttpResponseForbidden
from rest_framework.views import APIView

from app.internal.models.t_user import TelegramUser
from app.internal.serializers import TgUserSerializer


class TelegramUserGetView(APIView):
    http_method_names = ['get']

    def get(self, request, pk: int):
        db_user = TelegramUser.objects.filter(id=pk).first()

        if not db_user:
            return HttpResponseNotFound()

        if not db_user.phone:
            return HttpResponseForbidden()

        return JsonResponse(data={"user": TgUserSerializer(db_user).data})
