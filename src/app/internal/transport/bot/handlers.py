from app.internal.helpers import get_correct_phone
from app.internal.models.t_user import TelegramUser


def start(update, context):
    user = update.effective_user
    db_user, is_created = TelegramUser.objects.get_or_create(
        id=user.id,
        username=user.username,
        full_name=user.full_name,
    )

    message = 'Добро пожаловать' if is_created else 'С возвращением'
    update.message.reply_text(f"{message}, {user.full_name}!")


def set_phone(update, context):
    user = update.effective_user
    db_user = TelegramUser.objects.filter(id=user.id).first()

    if not db_user:
        update.message.reply_text('Сначала надо зарегаться ¯\_(ツ)_/¯')
        return

    try:
        db_user.phone = get_correct_phone(context.args[0])
        db_user.save()
        update.message.reply_text('Телефончик записал')
    except (IndexError, ValueError):
        update.message.reply_text('А корректный номер телефона где?')


def me(update, context):
    user = update.effective_user
    db_user = TelegramUser.objects.filter(id=user.id).first()

    if not db_user:
        update.message.reply_text('Зарегайся - /start')

    if not db_user.phone:
        update.message.reply_text('Заполни номер телефона - /set_phone')

    if all([db_user, db_user.phone]):
        update.message.reply_text(
            f'Инфа о тебе:\nid: {db_user.id}\nusername: {db_user.username}\n'
            f'имя: {db_user.full_name}\nномер телефона: {db_user.phone}\n'
        )
